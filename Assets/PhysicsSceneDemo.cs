﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PhysicsSceneDemo : MonoBehaviour
{
    public GameObject[] simulationObjects;
    public float physicsSceneTime;

    float mLastPhysicsSceneTime;
    Scene mPhysicsTestScene;
    PhysicsScene mPhysicsScene;


    void Start()
    {
        mPhysicsTestScene = SceneManager.CreateScene("PhysicsTestScene");
        mPhysicsScene = mPhysicsTestScene.GetPhysicsScene();

        for (int i = 0; i < simulationObjects.Length; i++)
            SceneManager.MoveGameObjectToScene(simulationObjects[i], mPhysicsTestScene);

        Physics.autoSimulation = false;
    }

    void Update()
    {
        if (physicsSceneTime != mLastPhysicsSceneTime)
        {
            mPhysicsScene.Simulate(physicsSceneTime);
            mLastPhysicsSceneTime = physicsSceneTime;
        }
    }

    void OnDestroy()
    {
        SceneManager.UnloadSceneAsync(mPhysicsTestScene);
        Physics.autoSimulation = true;
    }
}
